export function colorInit() {

    // Variable(s) de couleurs
    /*var colors = [
        '#1abc9c', 
        '#2ecc71', 
        '#3498db', 
        '#f39c12'
    ];*/
    
    // Couleur unique temporaire
    var colors = [
        '#52D28E'
    ];

    // Séléctionne une couleur Random dans la variable Array 'colors'
    var domiColor = colors[Math.floor(Math.random() * colors.length)];

    // Ecriture des règles CSS ajoutées pour la couleur sur tout les éléments
    var css = 
    '.domiC{color:' + domiColor + '}' +
    '.domiB{background-color:' + domiColor + '}' + 
    '.linkFooter:hover {color:' + domiColor + '}' +
    '.liLink a:before {color:' + domiColor + '}' +
    '.liLink a:after {color:' + domiColor + '}' +
    '::-webkit-scrollbar-thumb {background-color:' + domiColor + '}' +
    'a {color:' + domiColor + '}' +
    'input,select,textarea{color:' + domiColor + '!important}' +
    '.saveFolioBtn{background-color:' + domiColor + '!important}' +
    '.btn-large {background-color:' + domiColor + '!important}' +
    '.prefix.active {color:' + domiColor + '!important}' +
    '.inputContact {border-bottom: 1px solid ' + domiColor + '!important}' +
    '.helperContact.active {color:' + domiColor + '!important}'
    ;
    
    // Récupération du style ajouté pour la couleur
    var colorStyleElt = document.getElementsByClassName('color-style');
    
    // Si le style ajouté est déja présent
    if (colorStyleElt[0]){
        // Suppression du style
        colorStyleElt[0].remove();
        // Lancement de l'ajout du style
        loadStyle();
    } else {
        // Sinon premier affichage, lancement de l'ajout du style
        loadStyle();
    }

    // Fonction d'ajout du style pour la couleur dans Head
    function loadStyle(){
        var style = document.createElement('style');
        style.className = 'color-style';
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        document.getElementsByTagName('head')[0].appendChild(style);
    }

    

}