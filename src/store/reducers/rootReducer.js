import authReducer from './authReducer'
import folioReducer from './folioReducer'
import skillReducer from './skillReducer'
import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase'

const rootReducer = combineReducers({
  auth: authReducer,
  folio: folioReducer,
  skill: skillReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer
});

export default rootReducer